from typing import Union, Dict, Any, Optional
import hmac
import hashlib
import requests
import json

class EqonexApiClientRest:
    def __init__(self,
                 username: str = None,
                 password: str = None,
                 code_2fa: Union[int, str] = None,
                 testnet: bool = False) -> None:
        from warnings import warn
        warn("username and password login is deprecated, please consider switching to API key authentication using EqonexApiClientRestKeyAuth")

        self.base_endpoint = 'https://{}eqonex.com/api/'.format("testnet." if testnet else "")

        self.username = username
        self.password = password

        self.cookies = requests.cookies.RequestsCookieJar()

        if username is not None and password is not None:
            login_credentials = self._login_eqonex(username, password, code_2fa)
            self._internal_key_auth_client = EqonexApiClientRestKeyAuth(login_credentials['requestToken'],
                                                                        login_credentials['requestSecret'],
                                                                        login_credentials['id'],
                                                                        testnet)
        else:
            self._internal_key_auth_client = EqonexApiClientRestKeyAuth(testnet)

    def _login_eqonex(self,
                      username: str = None,
                      password: str = None,
                      code_2fa: Union[int, str] = None,
                      subaccount_id : Optional[Union[int, str]] = None) -> Dict[str, str]:
        data = {"login": username, "password": password}

        if code_2fa is not None:
            data['code'] = str(code_2fa)

        if subaccount_id is not None:
            data["account"] = subaccount_id

        endpoint = 'logon'
        login_request = requests.post("{}{}".format(self.base_endpoint, endpoint), json=data, cookies=self.cookies)
        self.cookies.update(login_request.cookies)

        if login_request.status_code != 200:
            EqonexApiClientRestKeyAuth._raise_request_error(login_request)

        login_credentials = login_request.json()

        if 'requestToken' not in login_credentials or 'requestSecret' not in login_credentials or 'id' not in login_credentials:
            raise KeyError('Login Failed')

        return login_credentials

    def __getattr__(self, attr):
        # Pass any method calls through to the internal client
        return getattr(self._internal_key_auth_client, attr)


class EqonexApiClientRestKeyAuth:
    def __init__(self,
                 requestToken: str = None,
                 requestSecret: str = None,
                 accountId: int = None,
                 testnet: bool = False) -> None:
        self.base_endpoint = 'https://{}eqonex.com/api/'.format("testnet." if testnet else "")
        self.testnet = testnet
        self.logged_in = False

        self.cookies = requests.cookies.RequestsCookieJar()

        if requestToken is not None and requestSecret is not None and accountId is not None:
            self.login_credentials = {"requestToken": requestToken, "requestSecret": requestSecret, "id": accountId}
            self.logged_in = True

    def _sign_api_request(self,
                          api_secret : str,
                          request_body : Dict[str, Any] = {}) -> str:
        signature_hash = hmac.new(api_secret.encode(), request_body.encode(), hashlib.sha384).hexdigest()
        return signature_hash

    def _public_endpoint(self,
                         endpoint: str,
                         data: Dict[str, Any] = {}) -> Dict[str, str]:
        query_string = "&".join(["{}={}".format(a, b) for a, b in data.items()])
        request = requests.get("{}{}?{}".format(self.base_endpoint, endpoint, query_string), cookies=self.cookies)
        self.cookies.update(request.cookies)

        if request.status_code != 200:
            EqonexApiClientRestKeyAuth._raise_request_error(request)

        return request.json()

    def _private_endpoint(self,
                          endpoint: str,
                          data: Dict[str, Any] = {},
                          subaccount_id : Optional[Union[int, str]] = None) -> Any:
        if not self.logged_in:
            raise KeyError("Cannot call private endpoints without logging in first")

        params = {**{"userId": self.login_credentials['id']}, **data}

        if subaccount_id is not None:
            params['account'] = subaccount_id

        requestbody = json.dumps(params, separators=(',', ':'))

        requestToken = self.login_credentials['requestToken']
        requestSecret = self.login_credentials['requestSecret']
        signature = self._sign_api_request(requestSecret, requestbody)

        headers = {'requestToken': requestToken, 'signature': signature}
        request = requests.post("{}{}".format(self.base_endpoint, endpoint), data=requestbody, headers=headers, cookies=self.cookies)
        self.cookies.update(request.cookies)

        if request.status_code != 200:
            EqonexApiClientRestKeyAuth._raise_request_error(request)

        return request

    @staticmethod
    def _raise_request_error(request):
        error_msg = "Request {} Failed with Code {}".format(request, request.status_code)
        print(error_msg)

        try:
            # In case this is running in iPython, try to format the received text nicely
            from IPython.core.display import display, HTML
            print()
            display(HTML(request.text))
        except Exception as e:
            print(request.text)
            pass

        raise KeyError(error_msg)


    #
    #  Public Endpoints
    #

    def getInstruments(self) -> Dict[str, str]:
        return self._public_endpoint('getInstruments')

    def getInstrumentPairs(self) -> Dict[str, str]:
        return self._public_endpoint('getInstrumentPairs')

    def getOrderBook(self,
                     pair_id: Union[int, str]) -> Dict[str, str]:
        data = {'pairId': pair_id}
        return self._public_endpoint('getOrderBook', data)

    def getTradeHistory(self,
                        pair_id: Union[int, str]) -> Dict[str, str]:
        data = {'pairId': pair_id}
        return self._public_endpoint('getTradeHistory', data)

    def getFundingRateHistory(self,
                              **kwargs: Optional[Any]) -> Dict[str, str]:
        data = kwargs
        return self._public_endpoint('getFundingRateHistory', data)

    def getChart(self,
                 pair_id: Union[int, str],
                 timespan: Union[int, str],
                 **kwargs: Optional[Any]) -> Dict[str, str]:
        data = {**{'pairId': pair_id, 'timespan': timespan}, **kwargs}
        return self._public_endpoint('getChart', data)

    #
    #  Private Endpoints
    #

    # Unbelievably slow and often fails for accounts with lots of orders
    def getOrders(self,
                  subaccount_id: Optional[Union[int, str]] = None,
                  **kwargs: Optional[Any]) -> Dict[str, str]:
        return self._private_endpoint('getOrders', kwargs, subaccount_id).json()

    def getOrderStatus(self,
                       orderId: Union[int, str],
                       subaccount_id: Optional[Union[int, str]] = None,
                       **kwargs: Optional[Any]) -> Dict[str, str]:
        data = kwargs
        data['orderId'] = orderId
        return self._private_endpoint('getOrderStatus', data, subaccount_id).json()

    def getOpenOrders(self,
                      subaccount_id: Optional[Union[int, str]] = None,
                      **kwargs: Optional[Any]) -> Dict[str, str]:
        return self._private_endpoint('getOpenOrders', kwargs, subaccount_id).json()

    def getOrderHistory(self,
                        orderId: Union[int, str],
                        subaccount_id: Optional[Union[int, str]] = None,
                        **kwargs: Optional[Any]) -> Dict[str, str]:
        data = kwargs
        data['orderId'] = orderId
        return self._private_endpoint('getOrderHistory', data, subaccount_id).json()

    def userTrades(self,
                   subaccount_id: Optional[Union[int, str]] = None,
                   **kwargs: Optional[Any]) -> Dict[str, str]:
        return self._private_endpoint('userTrades', kwargs, subaccount_id).json()

    def getPositions(self,
                     subaccount_id: Optional[Union[int, str]] = None,
                     **kwargs: Optional[Any]) -> Dict[str, str]:
        return self._private_endpoint('getPositions', kwargs, subaccount_id).json()

    def userFundingPayments(self,
                            subaccount_id: Optional[Union[int, str]] = None,
                            **kwargs: Optional[Any]) -> Dict[str, str]:
        kwargs_str = "{}{}".format("" if len(kwargs) == 0 else "&", "&".join(["{}={}".format(x, y) for x, y in kwargs.items()]))
        endpoint = "getUserHistory?type={}&format=json{}".format("fundingPayment", kwargs_str)
        funding_data = self._private_endpoint(endpoint, subaccount_id=subaccount_id)
        return json.loads(funding_data.text)

    def placeOrder(self,
                   client_order_id: str,
                   side: Union[int, str],
                   order_type: Union[int, str],
                   price: int,
                   price_scale: int,
                   quantity: int,
                   quantity_scale: int,
                   instrument_id: Union[int, str] = None,
                   symbol: str = None,
                   subaccount_id: Optional[Union[int, str]] = None,
                   **kwargs: Optional[Any]) -> Dict[str, str]:
        data = kwargs
        data['clOrdId'] = client_order_id
        data['side'] = side
        data['ordType'] = order_type
        data['price'] = price
        data['price_scale'] = price_scale
        data['quantity'] = quantity
        data['quantity_scale'] = quantity_scale

        if symbol is not None:
            data['symbol'] = symbol
        if instrument_id is not None:
            data['instrumentId'] = instrument_id

        return self._private_endpoint('order', data, subaccount_id).json()

    def cancelOrder(self,
                    instrument_id: Union[int, str],
                    original_order_id: Optional[str] = None,
                    client_order_id: Optional[str] = None,
                    subaccount_id: Optional[Union[int, str]] = None) -> Dict[str, str]:
        data = {'instrumentId': instrument_id}
        
        if original_order_id is not None:
            data['origOrderId'] = original_order_id
        elif client_order_id is not None:
            data['clOrdId'] = client_order_id
        else:
            raise KeyError('Must specify either original_order_id or client_order_id in cancelOrder')

        return self._private_endpoint('cancelOrder', data, subaccount_id).json()

    def cancelAll(self,
                  instrument_id: Optional[Union[int, str]] = None,
                  subaccount_id: Optional[Union[int, str]] = None) -> None:
        data = {}
        if instrument_id is not None:
            data['instrumentId'] = instrument_id

        return self._private_endpoint('cancelAll', data, subaccount_id)

    def getRisk(self,
                subaccount_id: Optional[Union[int, str]] = None,
                **kwargs: Optional[Any]) -> Dict[str, str]:
        return self._private_endpoint('getRisk', kwargs, subaccount_id).json()

    #
    #  Wallet Endpoints
    #

    def getDepositAddresses(self,
                            instrument_id: Union[int, str],
                            subaccount_id: Optional[Union[int, str]] = None,
                            **kwargs: Optional[Any]) -> Dict[str, str]:
        data = kwargs
        data['instrumentId'] = instrument_id
        return self._private_endpoint('getDepositAddresses', data, subaccount_id).json()

    def getWithdrawRequests(self,
                            subaccount_id: Optional[Union[int, str]] = None,
                            **kwargs: Optional[Any]) -> Dict[str, str]:
        return self._private_endpoint('getWithdrawRequests', kwargs, subaccount_id).json()

    def getTransferHistory(self,
                           subaccount_id: Optional[Union[int, str]] = None,
                           **kwargs: Optional[Any]) -> Dict[str, str]:
        return self._private_endpoint('getTransferHistory', kwargs, subaccount_id).json()
