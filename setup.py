import setuptools

setuptools.setup(
    name='eqonex',
    version='0.0.1',
    description='API clients for EQONEX exchange',
    author='Jack Gillett',
    author_email='jack.gillett@eqonex.com',
    packages=setuptools.find_packages(),
    install_requires=['websocket-client>=1.2.1', 'simplefix', 'requests', 'pandas']
)

