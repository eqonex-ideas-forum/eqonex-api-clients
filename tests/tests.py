import sys
import json
import pandas as pd
from eqonex import EqonexApiClientRest

testnet = int(sys.argv[1])
print("TestNet variable is {}".format(str(bool(testnet)).upper()))

client_rest = EqonexApiClientRest(testnet=testnet)

tests = [
    # PUBLIC ENDPOINTS
    {
        'endpoint': 'getInstruments',
        'test_name': 'getIntruments returns 12 instruments',
        'test_script': 'lambda x: len(x["instruments"]) == 12',
        'kwargs_str': '{}'
    },
    {
        'endpoint': 'getInstrumentPairs',
        'test_name': 'getIntrumentPairs returns 13 spot or perpetual cross pairs',
        'test_script': 'lambda x: len([x for x in x["instrumentPairs"] if x[8] in {"PAIR", "PERPETUAL_SWAP"}]) == 13',
        'kwargs_str': '{}'
    },
    {
        'endpoint': 'getOrderBook',
        'test_name': 'Check BTC orderbook has 2 BTC on BID side',
        'test_script': 'lambda x: (pd.DataFrame(x["bids"])[1].sum() / 1e6) > 2',
        'kwargs_str': '{"pair_id": 52}'
    },
    {
        'endpoint': 'getOrderBook',
        'test_name': 'Check BTC orderbook has 2 BTC on ASK side',
        'test_script': 'lambda x: (pd.DataFrame(x["asks"])[1].sum() / 1e6) > 2',
        'kwargs_str': '{"pair_id": 52}'
    },
    {
        'endpoint': 'getFundingRateHistory',
        'test_name': 'getFundingRateHistory returns records for instrumentId 25 and 29',
        'test_script': 'lambda x: set(pd.DataFrame(x["fundingRateHistory"])["instrumentId"]) == set([25, 29])',
        'kwargs_str': '{}'
    },
    {
        'endpoint': 'getChart',
        'test_name': 'getChart returns exactly 511 rows in the chart section',
        'test_script': 'lambda x: len(x["chart"]) == 511',
        'kwargs_str': '{"pair_id": 52, "timespan": 1, "limit": 511}'
    },
    {
        'endpoint': 'getTradeHistory',
        'test_name': 'getTradeHistory returns last 1000 trades for BTC',
        'test_script': 'lambda x: len(x["trades"]) == 1000',
        'kwargs_str': '{"pair_id": 52}'
    }
]

def apply_test_rest(client, endpoint, kwargs, test, test_name):

    # Pull the endpoint off the client, evaluate its data
    endpoint_function = getattr(client, endpoint)
    api_data = endpoint_function(**kwargs)

    # Evaluate the test result, assert true
    try:
        assert test(api_data), "TEST FAILED: {}".format(test_name)
    except KeyError as e:
        print("TEST FAILED: {}, BAD JSON: {}".format(test_name, api_data))
    
    print("Test Successful: {}".format(test_name))

for non_reg_test in tests:
    endpoint = non_reg_test['endpoint']
    test_name = non_reg_test['test_name']
    test_script = non_reg_test['test_script']
    kwargs_str = non_reg_test['kwargs_str']

    kwargs = json.loads(kwargs_str)
    test = eval(test_script)
    apply_test_rest(client_rest, endpoint, kwargs, test, test_name)
